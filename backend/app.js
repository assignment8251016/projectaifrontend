import express  from 'express';
import fileUpload from 'express-fileupload';
import bodyParser from 'body-parser'
import multer from 'multer'
import cors from "cors"
import fs from "fs"
const app = express();
const port = 3000;

// Use the express-fileupload middleware
app.use(express.json()); // Parse JSON-encoded bodies
app.use(cors()); // Parse JSON-encoded bodies

// Enable CORS for the server (if required)
// You can use the 'cors' package to handle CORS
app.post('/upload', (req, res) => {
  // Log the received data to the console (for debugging purposes)
  console.log(req.body);

  // Extract the imageBuffer from the request body
  const imageBuffer = req.body.imageBuffer;

  // If no image buffer submitted, return an error
  if (!imageBuffer) {
    return res.status(400).json({ error: 'No image data received' });
  }

  // Convert the base64-encoded image buffer to a Buffer object
  const image = Buffer.from(imageBuffer.replace(/^data:image\/\w+;base64,/, ''), 'base64');

  // Write the image buffer to a file (e.g., "uploaded_image.jpg")
  fs.writeFile('./uploaded_image.jpg', image, (err) => {
    if (err) {
      console.error('Error writing the image:', err);
      return res.status(500).json({ error: 'Error writing the image' });
    }

    console.log('Image successfully written to "uploaded_image.jpg"');
    res.json({ message: 'Image successfully uploaded' });
  });
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});