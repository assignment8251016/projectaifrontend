import React, { useState } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
const columns = [
  {
    name: <h2>Predictions</h2>,
    selector: row => row.title,
  },
];



const ImageUploader = () => {
  const [imagePreview, setImagePreview] = useState('');
  const [imageFile, setImageFile] = useState(null);
  const [prediction, setPrediction] = useState(undefined);

  let data = []
  

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImageFile(file);

    const reader = new FileReader();
    console.log("hello")

    reader.onloadend = () => {
      setImagePreview(reader.result);
      console.log(imagePreview)
    };
    reader.readAsDataURL(file);

  };


  const handleUpload = () => {
    // Convert the imageFile to a base64-encoded string
    if (imageFile) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const imageBuffer = reader.result; // This will be the base64-encoded image buffer

        // Send the imageBuffer to the backend using Axios
        axios.post('http://127.0.0.1:8000/index', { "imageBuffer": imageBuffer })
          .then((response) => {
            setPrediction(response.data.prediction.slice(1, -1).replace(/'/g, '').split(','))
            console.log('Image uploaded successfully:', response.data.prediction.slice(1, -1).replace(/'/g, '').split(','));
          })
          .catch((error) => {
            console.error('Error uploading image:', error);
          });
      };

      reader.readAsDataURL(imageFile);
    }
  };
  if (imageFile) {
    data = [{title: "Clear"}, {title: "Vegetation"}]
  }

  if (prediction) {
    data = prediction.map(d => ({
     title: d,
   }))
  }
  

  return (
    <div style={{'display':'flex', flexDirection:'column', alignItems:'center'}}>
      {imagePreview && <img style={{'padding':'20px'}} src={imagePreview} alt="Preview" />}
      <input style={{paddingBottom: '20px'}} type="file" name="uploadfile" accept="image/*" onChange={handleImageChange} />
      <button style={{marginBottom: '10px', backgroundColor:'purple'}} onClick={handleUpload}>Upload Image</button>
      {
        <DataTable
          columns={columns}
          data={data}
        />
      }
    </div>
  );
};



export default ImageUploader;
